﻿// See https://aka.ms/new-console-template for more information
using ClassArray;

Car[] garage = {new Car(101, "mustang", 2000, 2000000),
                new Car(102, "ferrari", 2002, 4000000),
                new Car(103, "honda", 2016, 7000000),
                new Car(104, "suzuki", 2019, 5000000),
                new Car(105, "BMW", 2022, 56600000)};

foreach(Car car in garage)
{
    Console.WriteLine("*******************************");
    Console.WriteLine($" Car ID:: {car.carId} \n Car Name :: {car.carName}\n Car Launch year :: {car.launchYear}\n Car price :: {car.carPrice}\n");






    Console.WriteLine("*******************************");
}
