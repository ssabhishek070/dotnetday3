﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArray
{
    internal class Car
    {
        internal int carId;
        internal string ?carName;
        internal int launchYear;
        internal int carPrice;

        public Car(int id,string name,int Year,int price)
        {
            this.carId = id;
            this.carName=name;
            this.carPrice = price;
            this.launchYear = Year;
        }
        
    }
   
}
