﻿using ClassArrayPrac.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArrayPrac.repository
{

    internal class ProductRepo
    {
        Product[] prod;
        public ProductRepo()
        {
            prod = new Product[]{
            new Product(101,"OPPO","Mobile",10000),
            new Product(102,"VIVO","Mobile",10000),
            new Product(103,"SAMSUNG","TV",1000),
            new Product(104,"LENEVO","Laptop",1000),

            };





        }

        public Product[] getAllProducts()
        {
            return prod;
        }
        public Product[] updateElement( int id,string name,string category,int price)
        {
            int ind = Array.FindIndex(prod, item => item.Id == id);
            prod[ind].Name = name;
            prod[ind].Category = category;
            prod[ind].Price = price;

            return prod;

        }
        public Product[] deleteElement(int id)
        {
            prod = Array.FindAll(prod, item => item.Id != id);
            return prod;
        }
        public void getProductByCatergory()
        {
            foreach(Product p in prod)
            {
                if (p.Category == "Mobile")
                {
                    Console.WriteLine("Product by category");
                    Console.WriteLine("********************************************");
                    Console.WriteLine($" Product Id :: {p.Id}\n Product Name :: {p.Name}\n Product Category :: {p.Category}\n Product Price ::{p.Price}");
                    Console.WriteLine("********************************************");
                }
            }
        }
    }
       
}
