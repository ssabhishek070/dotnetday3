﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassArrayPrac.model
{
    internal class Product
    {
        //public int Id ;
        //public string Name;

        //public string Category;

        //public int Price;
        public int Id;
        public string Name;
        public string Category;
        public int Price;


        public Product(int Id, string Name, string Category, int Price)
        {
           this.Id = Id;
           this.Name = Name;
            this.Category = Category;
            this.Price = Price;

        }
    }
}
